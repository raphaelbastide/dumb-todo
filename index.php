<?php
session_start();
$todos = [];
if (file_exists('todo.json')) {
  $json = file_get_contents('todo.json');
  $todos = json_decode($json, true);
  $todos = array_reverse($todos);
}
?>
<!doctype html>
<html lang="en">
<head>
  <title>TODO</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="robots" content="noindex, nofollow">
  <meta name="googlebot" content="noindex, nofollow">
  <link rel="shortcut icon" href="favicon.png"/>
  <style>
    :root{--c:MediumSpringGreen;}
    body{background:#222; color:var(--c); font-family:monospace; font-size:16px;min-height:100vh; margin:0;}
    main{padding:10px;}
    body.drag-enter{background:#05271a;}
    form{display:flex; flex-wrap:wrap;}
    .alert{width:100%;margin-bottom:4px; color:var(--c);}
    .edit{background:none; font-size:.9em; padding:0;}
    input{padding:.5em; font-family:monospace;}
    .add-form input, button{background:#000; border:0; color:inherit;}
    .add-form button{font-size:1.4em; padding:0px 6px; margin-left:4px;}
    .item, .btn-bar{display:flex;}
    .btn-bar{margin-right:.4em; width:2.5em; justify-content:space-between;}
    .btn-bar *{color:var(--c); }
    a{color:inherit; text-decoration:none;}
    form, p,.fav-list, .archive-list{margin: 4px 0;}
    .fav-list{color:var(--c); border:1px solid var(--c); padding:4px;}
    .archive-list{color:gray; border:1px solid gray; padding:4px;}
    .fav-list .fav, .archive-list .complete{opacity:.2; color:white;}
    summary{ padding:2px 4px;}
    .edit-form, .edited .title{display:none;}
    .edited .edit-form, .title{display:inline;}
  </style>
</head>
<body>
  <main>
    <!-- Form -->
    <form class="add-form" action="newTodo.php" method="post">
      <?php
      if (isset($_SESSION['AddMessage'])) {
        echo '
        <div class="alert">
        Successfully add todo!
        </div>';
        unset($_SESSION['AddMessage']);
      }
      ?>
      <?php
      if (isset($_SESSION['CompleteMessage'])) {
        echo '
        <div class="alert">
        Successfully completed todo!
        </div>';
        unset($_SESSION['CompleteMessage']);
      }
      ?>
      <input type="text" autofocus name="todo">
      <button type="submit">+</button>
    </form>

    <!-- Built a fav List -->
    <details class="fav-list">
      <summary>★</summary>
      <?php foreach ($todos as $todo): ?>
        <?php
          $id = $todo['id'];
          $name = $todo['content'];
          if (!isset($todo["fav"]) || $todo["fav"] == false) {
            continue;
          }
          $date = $todo['date'];
          if ($todo['completed']) {
            $status = "completed";
          }else{
            $status = "todo";
          }
          ?>
        <div class="item <?= $status; ?>">
          <div class="btn-bar">
            <a class="delete" title="Complete" href="completeTodo.php?todo=<?= $id; ?>">✔</a>
            <a class="fav" title="Unfav" href="favTodo.php?todo=<?= $id; ?>">★</a>
            <button class="edit" title="edit">↘</button>
          </div>
          <span class="title"><?= $name; ?></span>
          <form class="edit-form" action="editTodo.php" method="post">
            <input type="text" name="todo" value="<?= $name; ?>">
            <input type="text" name="id" hidden value="<?= $id; ?>"></input>
            <button hidden type="submit">ok</button>
          </form>
        </div>

      <?php endforeach; ?>
    </details>

    <!-- TODO list -->
    <?php foreach ($todos as $todo): ?>
      <?php
        $id = $todo['id'];
        $name = $todo['content'];
        if (isset($todo["fav"]) || $todo["completed"] == true) {
          continue;
        }
        $date = $todo['date'];
        if ($todo['completed']) {
          $status = "completed";
        }else{
          $status = "todo";
        }
        ?>
      <div class="item <?= $status; ?>">
        <div class="btn-bar">
          <a class="complete" title="Complete" href="completeTodo.php?todo=<?= $id; ?>">✔</a>
          <a class="fav" title="Favorite" href="favTodo.php?todo=<?= $id; ?>">★</a>
          <button class="edit" title="edit">↘</button>
        </div>
        <span class="title"><?= $name; ?></span>
        <form class="edit-form" action="editTodo.php" method="post">
          <input type="text" name="todo" value="<?= $name; ?>">
          <input type="text" name="id" hidden value="<?= $id; ?>"></input>
          <button hidden type="submit">ok</button>
        </form>
      </div>
    <?php endforeach; ?>

    <!-- Completed archive -->
    <details class="archive-list">
      <summary>⊠</summary>
      <?php foreach ($todos as $todo): ?>
        <?php
          $id = $todo['id'];
          $name = $todo['content'];
          if (!isset($todo["completed"]) || $todo["completed"] == false) {
            continue;
          }
          $date = $todo['date'];
          if ($todo['completed']) {
            $status = "completed";
          }else{
            $status = "todo";
          }
          ?>
        <div class="item <?= $status; ?>">
          <div class="btn-bar">
            <a class="complete" title="Reactivate" href="completeTodo.php?todo=<?= $id; ?>">↻</a>
            <a class="fav" title="Favorite" href="favTodo.php?todo=<?= $id; ?>">★</a>
          </div>
          <span class="title"><?= $name; ?></span>
        </div>
      <?php endforeach; ?>
    </details>

  </main>
  <script>
    let b = document.body
    let items = document.querySelectorAll(".item")
    items.forEach(item => {
      if (item.querySelector('.edit')) {
        let editBtn = item.querySelector('.edit')
        editBtn.onclick = function(){
          console.log(editBtn);
          if(item.classList.contains('edited')){
            item.classList.remove('edited')
          }else{
            item.classList.add('edited')
          }
        }
      }
    });

    // Drag image, upload
    b.addEventListener('dragover', handleDragOver);
    b.addEventListener('dragenter', handleDragEnter);
    b.addEventListener('drop', handleDrop);

    function handleDragOver(event) {event.preventDefault(); b.classList.add('drag-enter')}
    function handleDragEnter(event) {event.preventDefault();}
    function handleDrop(event) {
      event.preventDefault();
      b.classList.remove('drag-enter')
      const file = event.dataTransfer.files[0];
      const name = file.name
      if (file.type.startsWith('image/')) {
        const reader = new FileReader();
        reader.onload = function(e) {
          imagePath = e.target.result
          uploadImage(imagePath,name)
        };
        reader.readAsDataURL(file);
      } else {
        alert('Please drop an image file.');
      }
    }
    function uploadImage(imageData,name) {
      let xhr = new XMLHttpRequest()
      xhr.open('POST', 'save-image.php', true)
      xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded')
      xhr.onreadystatechange = function() {
        if (xhr.readyState === 4 && xhr.status === 200) {
          console.log(xhr.responseText)
        }else{
          let scriptElement = document.createElement('script');
          scriptElement.innerHTML = xhr.responseText;
          document.body.appendChild(scriptElement);
        }
      };
      xhr.send('name='+ name + '&imageData=' + encodeURIComponent(imageData))
    }

  </script>
</body>
</html>