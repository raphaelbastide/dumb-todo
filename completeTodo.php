<?php
if (isset($_GET['todo'])) {
  $todo = $_GET['todo'];
  $json = file_get_contents('todo.json');
  $jsonArray = json_decode($json, true);
  if (isset($jsonArray[$todo]["completed"]) && $jsonArray[$todo]["completed"] == true) {
    $jsonArray[$todo]["completed"] = false;
  }else{
    $jsonArray[$todo]["completed"] = true;
  }
    // $jsonArray[$todo]["completed"] = true;

  file_put_contents('todo.json', json_encode($jsonArray, JSON_UNESCAPED_UNICODE|JSON_PRETTY_PRINT));
  session_start();
  $_SESSION['CompletMessage'] = true;
  header('location: index.php');
}
?>