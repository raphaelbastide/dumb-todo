<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>JSON fixer</title>
</head>
<body>
    <h1>Stolon JSON fixer</h1>
    <a href="?update=true">Remove all old items</a><br>
    
    <?php
        if (isset($_GET['update']) && $_GET['update'] === 'true') {
            echo '<p>JSON files updated successfully!</p>';                    
            echo '<pre>';
            $filename = 'todo.json';
            if (file_exists($filename)) {
                $jsonData = file_get_contents($filename);
                $data = json_decode($jsonData, true);
                $filteredData = array_filter($data, function($item) {
                    return !$item['completed'];
                });
                $filteredJson = json_encode($filteredData, JSON_PRETTY_PRINT);
                file_put_contents($filename, $filteredJson);
                echo "The file has been updated successfully.";
            } else {
                echo "The file does not exist.";
            }
    }
    ?>
</body>
</html>
