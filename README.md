# Dumb todo

A very basic TODO app for my desktop and dumb phone (not so dumb as it can access a web page).

- add,
- edit,
- favorite,
- archive TODO items,
- drop images

## Requirements

- PHP
- `images/` should have write permission
- rename `demo-todo.json` to `todo.json`, and make sure it has write permission

## License

GNU GPL