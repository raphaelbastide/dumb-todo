<?php
if (isset($_POST['imageData']) && isset($_POST['name'])) {
  $data = $_POST['imageData'];
  $name = $_POST['name'];
  // echo $data;
  $ext = explode('/', mime_content_type($data))[1];
  $data = str_replace('data:image/'.$ext.';base64,', '', $data);
  $data = str_replace(' ', '+', $data);
  $imageData = base64_decode($data);
  $filename = 'images/' . $name;
  $file = fopen($filename, 'wb');
  fwrite($file, $imageData);
  fclose($file);
  // echo 'Image saved successfully as ' . $filename;
    // Output JavaScript code to set the value of the input field
  echo 'console.log(1);';
  echo 'document.querySelector(".add-form input").value = "' . $filename . '";';
} else {
  // echo 'No image data received.';
}
?>
