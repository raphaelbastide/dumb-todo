<?php
if (isset($_POST['todo']) && $_POST['todo'] != "") {
  $todoContent = trim(htmlspecialchars($_POST['todo']));
  $id = time();
  // Autolink URL and image paths
  $todoContent = preg_replace(
    "~(?:[[:alpha:]]+://[^<>[:space:]]+[[:alnum:]/]|images/[^<>[:space:]]+[[:alnum:]/]+)~",
    "<a href=\"\\0\">\\0</a>",
    $todoContent
);
  if (file_exists('todo.json')) {
    $json = file_get_contents('todo.json');
    $jsonArray = json_decode($json, true);
  } else {
    $jsonArray = [];
  }
  $jsonArray[$id] = ['id' => $id, 'completed' => false, 'date' => time(), 'content' => $todoContent];
  file_put_contents('todo.json', json_encode($jsonArray, JSON_UNESCAPED_UNICODE|JSON_PRETTY_PRINT));
  session_start();
  $_SESSION['AddMessage'] = true;
}
header('location: index.php');
?>