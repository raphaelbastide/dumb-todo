<?php
if (isset($_GET['todo'])) {
  $todo = $_GET['todo'];
  $json = file_get_contents('todo.json');
  $jsonArray = json_decode($json, true);
  if (isset($jsonArray[$todo]["fav"]) && $jsonArray[$todo]["fav"] == true) {
    $jsonArray[$todo]["fav"] = false;
  }else{
    $jsonArray[$todo]["fav"] = true;
  }

  file_put_contents('todo.json', json_encode($jsonArray, JSON_UNESCAPED_UNICODE|JSON_PRETTY_PRINT));
  session_start();
  header('location: index.php');
}
?>