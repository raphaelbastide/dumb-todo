<?php
if (isset($_POST['todo'])) {
  $todoID = $_POST['id'];
  $todo = $_POST['todo'];
  $json = file_get_contents('todo.json');
  $jsonArray = json_decode($json, true);
  $jsonArray[$todoID]["content"] = $todo;
  $jsonArray[$todoID]["date"] = time();
  file_put_contents('todo.json', json_encode($jsonArray, JSON_UNESCAPED_UNICODE|JSON_PRETTY_PRINT));
  session_start();
  $_SESSION['CompletMessage'] = true;
  header('location: index.php');
}
?>

